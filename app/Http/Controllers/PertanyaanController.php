<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul' => $request["judul"],
            'isi' => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan->all());
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($pertanyaan_id){
        $pertanyaanku = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        // dd($pertanyaanku);
        return view('pertanyaan.show', compact('pertanyaanku'));
    }

    public function edit($pertanyaan_id){
        $pertanyaanku = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        // dd($pertanyaanku);
        return view('pertanyaan.edit', compact('pertanyaanku'));
    }

    public function update($pertanyaan_id, Request $request){
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:255',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')
                     ->where('id', $pertanyaan_id)
                     ->update([
                         'judul' => $request['judul'],
                         'isi' => $request['isi']
                     ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil diupdate!');
    }

    public function destroy($pertanyaan_id){
        $pertanyaanku = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        // dd($pertanyaanku);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
