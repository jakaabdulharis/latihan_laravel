<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        // dd($request->all());
        $firstname      = $request->firstname;
        $lastname       = $request->lastname;
        $gender         = $request->gender;
        $nationality    = $request->nationality;
        $language       = $request->language;
        $bio            = $request->bio;
        
        return view('hasil', ["firstname" => $firstname,"lastname" => $lastname]);
        // dd($bio);
    }
}

