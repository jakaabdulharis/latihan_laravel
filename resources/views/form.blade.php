<!DOCTYPE html>
<html>
<head>
	<title>Buat Account Baru
	</title>
</head>
<body>

	<h1>Buat Account Baru!</h1>

	<h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
		<label>First Name:</label><br><br>
		<input type="text" name="firstname"><br><br>
		<label>Last Name:</label><br><br>
		<input type="text" name="lastname">	<br><br>
		<label>Gender:</label><br>
		<input type="radio" name="gender" value="Male">Male<br>
		<input type="radio" name="gender" value="Female">Female<br>
		<input type="radio" name="gender" value="Other">Other<br><br>

		<label>Nationality:</label> <br><br>
		<select name="nationality">
			<option value="Indonesian">Indonesian</option>
			<option value="American">American</option>
			<option value="Australian">Australian</option>
		</select><br><br>

		<label>Language Spoken:</label> <br><br>
		<input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
		<input type="checkbox" name="language" value="English">English<br>
		<input type="checkbox" name="language" value="Other">Other<br><br>

		<label>Bio:</label> <br><br>
		<textarea name="bio" cols="30" rows="10"></textarea><br>

		<input type="submit" name="signup" value="Sign Up">		

		
	</form>

</body>
</html>