@extends('adminlte.master')

@section('content')
<div class="mt3 ml3">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Table Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif  
            <a href="/pertanyaan/create" class="btn btn-primary mb-2">Buat Pertanyaan Baru</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Title</th>
                <th>Body</th>
                <th style="width: 40px">Label</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key => $pertanyaanku)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$pertanyaanku -> judul}}</td>
                        <td>{{$pertanyaanku -> isi}}</td>
                        <td style="display: flex">
                            <a href="/pertanyaan/{{$pertanyaanku->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/pertanyaan/{{$pertanyaanku->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            <form action="/pertanyaan/{{$pertanyaanku->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                <tr>
                    <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                </tr>
                @endforelse
              {{-- <tr>
                <td>1.</td>
                <td>Update software</td>
                <td>
                  <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                  </div>
                </td>
                <td><span class="badge bg-danger">55%</span></td>
              </tr> --}}
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> --}}
      </div>
</div>
@endsection